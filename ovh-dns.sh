#!/bin/bash
echo "Scripts instal ovh-dns wrapper for dns manipulation"
echo "OVH App key, secret key and consumer key for /domain/* manipulation is required!"
echo "To generate keys use './ovh-dns.sh -A' script installs npm throug apt update"

# Options and arguments
while getopts a:c:s:d:AN:LCDSR:t:T: option
do
 case "${option}" in
   a) appKey=${OPTARG};;
   c) consumerKey=${OPTARG};;
   s) secretKey=${OPTARG};;
   d) domain=${OPTARG};;
   t) typeRecord=${OPTARG};;
   T) target=${OPTARG};;
   A) apiSetup=true;;
   R) record=${OPTARG};;
   L) list=true;;
   C) createRecord=true;;
   D) deleteRecord=true;;
   S) silent=true;;
 esac
done

if [ $silent ]; then
  echo 'Silent is on...'
  sf="-y"
fi 

# If initial setup
if [ $apiSetup ]; then

    if [ $(command -v jq) ]; then
      echo "JQ is already installed!"
    else
      echo "JQ is needed to grep json response!"
      sudo apt-get install jq $sf
    fi

    echo "Create application at:"
    echo "' https://eu.api.ovh.com/createApp/ ', if you already have app key enter it here."

    if [ "$appKey" = "" ]; then
      echo "Application key:"
      read appKey
    fi

    if [ "$appKey" = "" ]; then
      echo "Application key is required!"
      exit 1
    fi

    if [ "$secretKey" = "" ]; then
      echo "Secret key:"
      read secretKey
    fi

    if [ "$secretKey" = "" ]; then
      echo "Secret key is required!"
      exit 1
    fi

    mkdir -p $PWD/credentials

    if [ "$consumerKey" = "" ]; then
      # Link an app
      curl -XPOST -H"X-Ovh-Application: $appKey" -H "Content-type: application/json" \
      https://eu.api.ovh.com/1.0/auth/credential  -d '{
        "accessRules": [
            {
                "method": "GET",
                "path": "/domain/*"
            },
            {
                "method": "POST",
                "path": "/domain/*"
            },
            {
                "method": "PUT",
                "path": "/domain/*"
            },
            {
                "method": "DELETE",
                "path": "/domain/*"
            }
        ]
      }' | grep credential > $PWD/credentials/response

      cat $PWD/credentials/response | jq -r '.consumerKey' > $PWD/credentials/consumer.key
      consumerKey=$(cat $PWD/credentials/consumer.key)

    fi
    echo ""
    echo "*************************************************************"
    echo "application credentials are written in ./credentials folder:"
    echo ""
    echo "Application key:"
    echo $appKey
    echo $appKey > $PWD/credentials/application.key
    echo "Application secret:"
    echo $secretKey
    echo $secretKey > $PWD/credentials/secret.key
    echo "Consumer key:"
    echo $(cat $PWD/credentials/consumer.key)
    echo ""
    echo "**All_Done**"
    echo "To confirm visit:"
    echo $(cat $PWD/credentials/response | jq -r '.validationUrl')

    exit 0

fi

# Set keys
if [ "$domain" == "" ]; then

  echo "Continue? y/n"
  read ok

  if [ "$ok" == "n" -o "$ok" == 'n' ]; then

    echo "You aborted."
    exit 1

  fi
fi

if [ ! $createRecord -o ! $deleteRecord ]; then
  list=true
fi

if [ -z $domain ]; then
  echo "Enter domain name dns hosted at ovh:"
  read domain
fi

if [ -f $PWD/credentials/application.key ]; then
  appKey=$(cat $PWD/credentials/application.key)
else
  echo "Enter application key:"
  read appKey
fi

if [ -f $PWD/credentials/secret.key ]; then
  secretKey=$(cat $PWD/credentials/secret.key)
else
  echo "Enter secret key:"
  read secretKey
fi

if [ -f $PWD/credentials/consumer.key ]; then
  consumerKey=$(cat $PWD/credentials/consumer.key)
else
  echo "Enter consumer key:"
  read consumerKey
fi



node_modules() {

  if [ ! -d $HOME/.npm/ovh-dns-client ]; then
    echo "Installing ovh-dns-client globaly"
    sudo npm install ovh-dns-client -g
  else
    echo "$HOME/.npm/ovh-dns-client already exists."
  fi

}



checkNpm() {


  if which npm; then

    echo "NPM path: $(which npm)"
    echo "NPM version: $(npm -v)"
    node_modules

  else

    echo "NPM is needed! Install npm?"
    sudo apt-get install npm $sf
    sudo ln -s /usr/bin/nodejs /usr/bin/node
    node_modules

  fi


}


ovhDnsClient() {

  echo "Init client:"
  echo "Key: $appKey, secret key: $secretKey, consumer key: $consumerKey"
  echo "Record: $record, type of record: $typeRecord, target: $target"


  # LIST RECORDS
  if [ $list ]; then

    ovhDNS -K "$appKey" -S "$secretKey" -C "$consumerKey" records $domain    

  fi

  # CREATE RECORDS
  if [ $createRecord ]; then

    if [ "$record" == "" ]; then

      echo "Enter record name for $domain:"
      read record

    fi

    if [ "$typeRecord" == "" ]; then

      echo "Enter type of record (A, AAAA, TXT ...) for $domain:"
      read typeRecord

    fi

    if [ $silent ]; then

      echo "Silent is $silent: Target used is $(curl -4 http://icanhazip.com)"
      target=$(curl -4 http://icanhazip.com)

    elif [ "$target" == "" ]; then
      
      echo "Enter TARGET for record at $domain:"
      read target

    fi

    ovhDNS -K "$appKey" -S "$secretKey" -C "$consumerKey" create $domain "$record" $typeRecord "$target"

  fi

  # DELETE RECORDS
  if [ $deleteRecord ]; then


    if [ "$record" == "" ]; then

      echo "Enter record name to DELETE for $domain:"
      read record

    fi

    if [ "$typeRecord" == "" ]; then

      echo "Enter type of record to DELETE (A, AAAA, TXT ...) for $domain"
      read typeRecord

    fi



    if [ "$target"  == "" ]; then 

      echo "Enter TARGET to DELETE for record at $domain:"
      read target

    else 

      echo "Silent is $silent: Target used is $(curl -4 http://icanhazip.com)"
      target=$(curl -4 http://icanhazip.com)


    fi

    ovhDNS -K "$appKey" -S "$secretKey" -C "$consumerKey" delete $domain "$record" $typeRecord "$target"

  fi

}

#### LOGIC

checkNpm
#ovhDns
ovhDnsClient

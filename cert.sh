#!/bin/bash
echo "Scripts configures certificates for domain"

# Options and arguments
while getopts d:r:e:s option
do
 case "${option}" in
   d) domain=${OPTARG};;
   r) docRoot=${OPTARG};;
   e) email=${OPTARG};;
   s) silent=true;;
 esac
done

# Check if domain dns exists
check=false
retry=0
while [ $check == false ]; do
  if dig $domain | grep SOA; then
    echo Domain A record not found! Retrying in 10 seconds.
    sleep 30
    retry=$((retry + 1))
    echo "Retry #$retry"
  else
    echo "Domain A record found :)"
    sleep 10
    if curl http://$domain | grep "I am empty..."; then
      check=true
    fi
  fi
done


if [ "$silent" -a "$domain" != "" -a "$email" != "" ]; then

  echo 'Silent is on...'
  sf="-y"

else

  echo "Continue? y/Y"
  read c1

  if [ "$c1" == "y" -o "$c1" == 'Y' ]; then

    # Domain
    echo "Enter domain name to make certificates for."
    read domain

    echo "Enter doc root for challenge if empty /var/www/${domain} will be used."
    read docRoot

    # Email
    echo "Enter email. if empty none@none.com will be used:"
    read email

  else

    echo "You aborted."
    exit 1

  fi
fi

# Document root
if [ "$docRoot" == "" ]; then
    docRoot="/var/www/${domain}"
fi

if [ "$email" == "" ]; then
  email="none@none.com"
fi

# Certbot certificates


mainrun() {

  if cat $PWD/dry | grep successful; then
    sudo certbot certonly --webroot -w ${docRoot} -d ${domain} --agree-tos --non-interactive --rsa-key-size 4096 --email ${email}
    rm $PWD/dry
  else 
    dryrun
  fi

}

dryrun() {

  sudo certbot certonly --webroot -w ${docRoot} -d ${domain} --agree-tos --non-interactive --rsa-key-size 4096 --email ${email} --dry-run | grep successful > $PWD/dry
  mainrun

}

dryrun

sudo sed -i "s,/path/to/certificate.crt,/etc/letsencrypt/live/${domain}/cert.pem,g" /etc/apache2/sites-available/${domain}-https.conf
sudo sed -i "s,/path/to/certificate.key,/etc/letsencrypt/live/${domain}/privkey.pem,g" /etc/apache2/sites-available/${domain}-https.conf

sudo a2enmod ssl
sudo a2ensite ${domain}-https
sudo /etc/init.d/apache2 reload


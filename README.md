# Apache2 with configuration and certbot installation on ubuntu 16.04 LTS

## Basic installation

install.sh script:

	arguments:
	- (s)ilent
	- (d)omain


For basic installation run:

	./install.sh

For silent:

	./install.sh -sd "**www.loli.pop**"


## Create site

create-site.sh

	arguments:
	- (d)omain
    - document(r)oot
	- (s)ilent


For silent installation:

	./create-site.sh -d "**www.loli.pop**" -s


Creates /var/www/www.loli.pop document root and /etc/apache2/sites-available/www.loli.pop-*.*conf

## Certbot

cert.sh

	arguments:
    - (d)omain
    - document(r)oot
	- (e)mail	
	- (s)ilent

for silent domain and email is required default doc root will be used:

	./cert.sh -d www.loli.pop -s


or use all attributes

	./cert.sh -d www.loli.pop -r /var/www/www.loli.pop -e none@none.com

## OVH-DNS API

Script installs and uses ovh-dns-api to manipulate DNS entries hosted on OVH Dns.

For token generation use:

	./ovh-dns.sh -A

Access tokens are saved to credentials folder.

If credentials exist you can:

List entries:

	./ovh-dns.sh -d example.com -L

Create entry:

	./ovh-dns.sh -d example.com -C -R "subdomain" -t "A" -T "77.77.77.77"

Delete entry (same as create but D flag instead C):

	./ovh-dns.sh -d example.com -D -R "subdomain" -t "A" -T "77.77.77.77"


If silent Target server IP will be used!

	./ovh-dns.sh -d example.com -D -R "subdomain" -t "A" -S
	
# Full step with ovh-dns

    ./create-site.sh -d "test1.migo.si" -s # Create /var/www/test1.migo.si document root and /etc/apache2/sites-available/test1.migo.si-*.*conf in silent mode
    ./ovh-dns.sh -d migo.si -C -R "test1" -t "A" -S # Create DNS entry with server IP v4
    ./cert.sh -d test1.migo.si -e none@none.com -s # Create letsencrypt cert with default docroot http chalenge
    



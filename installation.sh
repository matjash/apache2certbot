#!/bin/bash
echo "Script installs apache2 with clean configuration and certbot."
echo "For -s(ilent) mode add required -d(omain)"

# Options and arguments
while getopts sd: option
do
 case "${option}" in
   d) domain=${OPTARG};;
   s) silent=true;;
 esac
done

# If silent
if [ "$silent" -a "$domain" != "" ]; then 

  echo 'Silent is on...'
  sf="-y"

else

  echo "Continue? y/Y"
  read c1

  if [ "$c1" == "y" -o "$c1" == 'Y' ]; then

    echo "Enter domain name for apache2.conf."
    read domain

  else

    echo "You aborted."
    exit 1

  fi
fi


init() {

  echo "Updating repositories..."
  sudo apt-get update && sudo apt-get upgrade ${sf}

} 

apache2() {

  sudo apt-get install apache2 ${sf}

  sudo cp $PWD/apache2.conf /etc/apache2/apache2.conf

  # replace domain name
  if [ "$domain" != "" ]; then

    sudo sed -i "s,www.example.com,${domain},g" /etc/apache2/apache2.conf

  fi

}

certbot() {

  sudo apt-get install software-properties-common ${sf}
  sudo add-apt-repository ppa:certbot/certbot ${sf}
  sudo apt-get update
  sudo apt-get install python-certbot-apache ${sf}

  # Cron job
  if crontab -l | grep "certbot"; then
   echo "Cron already exists."
  else
    if [ $silent ]; then

      crontab -l > mycron
      echo "43 6 * * * certbot renew --post-hook 'systemctl reload apache2'" >> mycron
      crontab mycron

    else

      echo "Make cronjob for certificate autorenewal?"
      read c2

      if [ "$c2" == "y" -o "$c2" == 'Y' ]; then

        crontab -l > mycron
        echo "43 6 * * * certbot renew --post-hook 'systemctl reload apache2'" >> mycron
        crontab mycron

      fi
    fi
    
    rm $PWD/mycron  
 
  fi
  
}


init
apache2
certbot

#!/bin/bash
echo "Script creates document root and basic configuration for site in sites-available."

# Options and arguments
while getopts d:r:s option
do
 case "${option}" in
   d) domain=${OPTARG};;
   r) docRoot=${OPTARG};;
   s) silent=true;;
 esac
done


# If silent
if [ "$silent" -a "$domain" != "" ]; then 

  echo 'Silent is on...'
  sf="-y"

else

  echo "Continue? y/Y"
  read c1

  if [ "$c1" == "y" -o "$c1" == 'Y' ]; then

    if [ ! $domain ]; then

      echo "Enter domain name."
      read domain

    fi

  else

    echo "You aborted."
    exit 1

  fi
fi


copyConf() {

  sudo cp $PWD/sites-available/000-default.conf /etc/apache2/sites-available/${domain}.conf
  sudo cp $PWD/sites-available/default-ssl.conf /etc/apache2/sites-available/${domain}-https.conf

  # replace domain name
  if [ "$domain" != "" ]; then

    sudo sed -i "s,www.example.com,${domain},g" /etc/apache2/sites-available/${domain}.conf
    sudo sed -i "s,www.example.com,${domain},g" /etc/apache2/sites-available/${domain}-https.conf

  fi

  # replace doc root
  if [ "$docRoot" == "" ]; then
    docRoot="/var/www/${domain}"
  fi

  sudo sed -i "s,/path/to/www,${docRoot},g" /etc/apache2/sites-available/${domain}.conf
  sudo sed -i "s,/path/to/www,${docRoot},g" /etc/apache2/sites-available/${domain}-https.conf

}

makeIndex() {
  (cat <<- EOF
I am empty...
EOF
  ) > $1/index.html
  

}

createDocRoot() {
  
  if [ "$docRoot" == "" ]; then
    docRoot="/var/www/${domain}"
  fi

  sudo mkdir -p ${docRoot}
  sudo chown $(whoami):www-data ${docRoot} -R
  makeIndex "${docRoot}"



}

enableHttpSite() {

  sudo a2ensite ${domain} > /dev/null 2>&1
  sudo /etc/init.d/apache2 reload

}

copyConf
createDocRoot
enableHttpSite

echo "Done :)"
